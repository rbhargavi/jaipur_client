# -*- coding: utf-8 -*-
{
    'name': 'Jaipur Client',
    'version': '1.0',
    'sequence': 145,
    'summary': 'Knachtechs Solutions',
    'category': 'Jaipur',
    'description': """
""",
    'depends': ['base','sale'],
    'data': [
        'views/ftp_config_view.xml'
    ],
    'installable': True,
    'application': True,
}
