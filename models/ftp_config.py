# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from ftplib import FTP
import os
from odoo.exceptions import RedirectWarning


class FtpConfig(models.Model):
    _name="ftp.config.settings"
    _rec_name="user_name"
    
    host=fields.Char(string="Host")
    user_name=fields.Char(string="User Name")
    password=fields.Char(string="Password")
    port=fields.Integer(string="Port Number")
    
    @api.multi
    def connect_to_ftp(self):
        for server in self:
            try:    
#                connect with host
                ftp = FTP(server.host)
#                create login with username and pass
                conn=ftp.login(server.user_name,server.password)
#                location to download file
                target_dir = '/home/knacktechs/Desktop'
#                look in this directory on server
                ftp.cwd('test_file')
#                matches all xls files
                filematch = '*.xls'
#                list out all the files
                files= ftp.nlst(filematch)
                for file in files:
                    target_file_name = os.path.join(target_dir,os.path.basename(file))
                    with open(target_file_name,'wb') as newfile:
                         ftp.retrbinary('RETR %s' % file, newfile.write)
            except Exception as e:
                raise RedirectWarning(_('Something wrong.')) 
            
        